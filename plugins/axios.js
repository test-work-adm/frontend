import VueCookies from 'vue-cookies'
import axios from "axios"
import { Notification } from 'element-ui';
import qs from 'qs';

const instance = axios.create({
    baseURL: '/api',
    paramsSerializer: { serialize: params => qs.stringify(params, { arrayFormat: 'repeat' }) },
})

instance.interceptors.request.use(function (config) {
  const token = VueCookies.get('taska_token')
  if (token) {
    config.headers['Authorization'] = 'Bearer ' + token
  }

  return config;
}, function (error) { 
  return Promise.reject(error);
});


instance.interceptors.response.use(function (response) { 
  return response;
}, function (error) {
  console.log(error.response.status)
  Notification({
    title: 'Ошибка',
    message: error.response.data.message ? error.response.data.message : 'Что-то пошло не так',
    type: 'error'
  })
  if (error.response.status === 401) {
    setTimeout(() => {
      window.location = '/login'
    }, 1000)
  }
  return Promise.reject(error);
});

export default instance
