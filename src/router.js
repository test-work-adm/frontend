import Vue from "vue"
import Router from "vue-router"
import HomeView from "@/views/HomeView/HomeView"  
import LoginView from "@/views/LoginView" 
import InformationView from "@/views/InformationView"  

Vue.use(Router)

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/login",
            component: LoginView,
            meta: { 
                login: true
            }
       },
        {
            path: "/",
            component: HomeView,
            meta: {
                index: '1'
            }
        }, 
       {
            path: "/info",
            component: InformationView,
            meta: {
                index: '2'
            }
       }
    ]
})