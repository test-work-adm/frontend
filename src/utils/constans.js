export const LANGUAGES = [
    {
      value: 'ru',
      label: 'Русский'
    },
     {
      value: 'en',
      label: 'Английский'
    },
  ]

  export const RULE_ACTIVE_STATES = [
    {
      value: '1',
      label: 'Активно'
    },
     {
      value: '0',
      label: 'Скрыто'
    },
  ]