import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router.js'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueCookies from 'vue-cookies' 
import accessPlugin from './plugins/access'
import lang from 'element-ui/lib/locale/lang/ru-RU';
import locale from 'element-ui/lib/locale';

Vue.config.productionTip = false

locale.use(lang)
Vue.use(ElementUI)
Vue.use(VueCookies)
Vue.use(accessPlugin)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
