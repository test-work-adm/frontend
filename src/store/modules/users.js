import instance from "../../../plugins/axios.js"
import VueCookies from 'vue-cookies'

export default {
    namespaced: true,
    state: {
        user: null
    },

    actions: { 

        async loginAction(ctx, payload) {
            const response = await instance.post('/users/login', payload) 
            VueCookies.set('taska_token', response.data)
            return response.data 
        },
        
        logoutAction() {
            VueCookies.remove('taska_token')
        },

        async fetchMeAction(ctx) { 
            const response = await instance.get('/users/me') 
            ctx.commit('SET_USER', response.data) 
            return response.data
        }

    },


    mutations: { 

        SET_USER(state, data) {
            state.user = data    
        }

    },

    getters: { 

        userRolesGetter(state) {
            if (!state.user) {
                return
            } 
            return state.user.rights
        }

    }

}