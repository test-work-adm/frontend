import instance from "../../../plugins/axios.js"
import moment from "moment"

export default {
    namespaced: true,
    state: {
        rules: { data: [] },
        loadingRules: false
    },
    actions: {
        async fetchRulesAction(ctx, filters) { 
            ctx.commit('SET_LOADING_RULES', true)
            const response = await instance.get('/rules', { params: filters }) 
            ctx.commit('SET_RULES', response.data)
            ctx.commit('SET_LOADING_RULES', false)
            return response.data
        },

        async changeActiveAction(ctx, obj) {  
            const response = await instance.patch(`/rules/active/${obj.id}`, { active: obj.active }) 
            return response.data
        },

        async deleteRuleAction(ctx, id) {  
            const response = await instance.delete(`/rules/delete/${id}`) 
            return response.data
        },

        async createRuleAction(ctx, obj) {
            const response = await instance.post('/rules/add/', obj) 
            return response.data
        }, 
       
        async updateRuleAction(ctx, obj) {
            const response = await instance.put('/rules/update/', obj) 
            return response.data
        }, 

    },
    mutations: { 

        SET_RULES(state, response){
            response.data.reduce( function(acc, item){ 
                return item.created = moment(item.created).format("DD-MM-YY HH:mm")
              }, 0); 
            state.rules = response 
        },

        SET_LOADING_RULES(state, loading) {
            state.loadingRules = loading
        }
    },

    getters: {

         getRules(state) {  
            return state.rules ? state.rules : []
        },

        loadingRulesGetter(state) {
            return state.loadingRules 
        }
        
    },
}