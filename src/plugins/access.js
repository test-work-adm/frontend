import store from '../store'

export default {
    install: (app) => {
      app.prototype.$access = (role) => {
        role = [role].flat()   
        const storeRoles = store.getters['users/userRolesGetter']   
        const result = role.some((el) => {return storeRoles[el]}) 
        return result
      }
    }
  }