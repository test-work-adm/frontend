import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from './views/HomeView/HomeView.vue' 
import LoginView from './views/LoginView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView 
  },
  {
    path: '/login', 
    component: LoginView
  }
]

const router = new VueRouter({
  routes
})

export default router
